﻿#pragma once
#include<math.h>
namespace ProjectCalculator {
	
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Calculator
	/// </summary>
	public ref class Calculator : public System::Windows::Forms::Form
	{
	public:
		Calculator(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Calculator()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  display;
	protected:

	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::Button^  button8;
	private: System::Windows::Forms::Button^  button9;
	private: System::Windows::Forms::Button^  button10;

	private: System::Windows::Forms::Button^  button12;
	private: System::Windows::Forms::Button^  button13;
	private: System::Windows::Forms::Button^  button14;
	private: System::Windows::Forms::Button^  button15;

	private: System::Windows::Forms::Button^  button17;
	private: System::Windows::Forms::Button^  button18;
	private: System::Windows::Forms::Button^  button19;
	private: System::Windows::Forms::Button^  button20;

	private: System::Windows::Forms::Button^  button22;
	private: System::Windows::Forms::Button^  button23;
	private: System::Windows::Forms::Button^  button24;
	private: System::Windows::Forms::Button^  button25;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->display = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->button13 = (gcnew System::Windows::Forms::Button());
			this->button14 = (gcnew System::Windows::Forms::Button());
			this->button15 = (gcnew System::Windows::Forms::Button());
			this->button17 = (gcnew System::Windows::Forms::Button());
			this->button18 = (gcnew System::Windows::Forms::Button());
			this->button19 = (gcnew System::Windows::Forms::Button());
			this->button20 = (gcnew System::Windows::Forms::Button());
			this->button22 = (gcnew System::Windows::Forms::Button());
			this->button23 = (gcnew System::Windows::Forms::Button());
			this->button24 = (gcnew System::Windows::Forms::Button());
			this->button25 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// display
			// 
			this->display->BackColor = System::Drawing::Color::White;
			this->display->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->display->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 19.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->display->Location = System::Drawing::Point(12, 20);
			this->display->Name = L"display";
			this->display->Size = System::Drawing::Size(399, 61);
			this->display->TabIndex = 0;
			this->display->Text = L"0";
			this->display->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 13.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button1->Location = System::Drawing::Point(12, 259);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(62, 53);
			this->button1->TabIndex = 1;
			this->button1->Text = L"%";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Calculator::button1_Click);
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 13.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button2->Location = System::Drawing::Point(89, 100);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(62, 53);
			this->button2->TabIndex = 2;
			this->button2->Text = L"CE";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Calculator::button2_Click);
			// 
			// button3
			// 
			this->button3->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 13.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button3->Location = System::Drawing::Point(169, 100);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(62, 53);
			this->button3->TabIndex = 3;
			this->button3->Text = L"C";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Calculator::button3_Click);
			// 
			// button4
			// 
			this->button4->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button4->Location = System::Drawing::Point(262, 100);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(62, 53);
			this->button4->TabIndex = 4;
			this->button4->Text = L"←";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Calculator::button4_Click);
			// 
			// button5
			// 
			this->button5->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button5->Location = System::Drawing::Point(12, 338);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(62, 53);
			this->button5->TabIndex = 5;
			this->button5->Text = L"÷";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Calculator::button5_Click);
			// 
			// button6
			// 
			this->button6->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button6->Location = System::Drawing::Point(12, 177);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(62, 53);
			this->button6->TabIndex = 6;
			this->button6->Text = L"√";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &Calculator::button6_Click);
			// 
			// button7
			// 
			this->button7->BackColor = System::Drawing::SystemColors::Window;
			this->button7->Location = System::Drawing::Point(99, 177);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(62, 53);
			this->button7->TabIndex = 7;
			this->button7->Text = L"7";
			this->button7->UseVisualStyleBackColor = false;
			this->button7->Click += gcnew System::EventHandler(this, &Calculator::button7_Click);
			// 
			// button8
			// 
			this->button8->BackColor = System::Drawing::SystemColors::Window;
			this->button8->Location = System::Drawing::Point(183, 177);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(62, 53);
			this->button8->TabIndex = 8;
			this->button8->Text = L"8";
			this->button8->UseVisualStyleBackColor = false;
			this->button8->Click += gcnew System::EventHandler(this, &Calculator::button8_Click);
			// 
			// button9
			// 
			this->button9->BackColor = System::Drawing::SystemColors::Window;
			this->button9->Location = System::Drawing::Point(262, 177);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(62, 53);
			this->button9->TabIndex = 9;
			this->button9->Text = L"9";
			this->button9->UseVisualStyleBackColor = false;
			this->button9->Click += gcnew System::EventHandler(this, &Calculator::button9_Click);
			// 
			// button10
			// 
			this->button10->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button10->Location = System::Drawing::Point(349, 177);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(62, 53);
			this->button10->TabIndex = 10;
			this->button10->Text = L"×";
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Click += gcnew System::EventHandler(this, &Calculator::button10_Click);
			// 
			// button12
			// 
			this->button12->BackColor = System::Drawing::SystemColors::Window;
			this->button12->Location = System::Drawing::Point(99, 257);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(62, 53);
			this->button12->TabIndex = 12;
			this->button12->Text = L"4";
			this->button12->UseVisualStyleBackColor = false;
			this->button12->Click += gcnew System::EventHandler(this, &Calculator::button12_Click);
			// 
			// button13
			// 
			this->button13->BackColor = System::Drawing::SystemColors::Window;
			this->button13->Location = System::Drawing::Point(183, 257);
			this->button13->Name = L"button13";
			this->button13->Size = System::Drawing::Size(62, 53);
			this->button13->TabIndex = 13;
			this->button13->Text = L"5";
			this->button13->UseVisualStyleBackColor = false;
			this->button13->Click += gcnew System::EventHandler(this, &Calculator::button13_Click);
			// 
			// button14
			// 
			this->button14->BackColor = System::Drawing::SystemColors::Window;
			this->button14->Location = System::Drawing::Point(262, 257);
			this->button14->Name = L"button14";
			this->button14->Size = System::Drawing::Size(62, 53);
			this->button14->TabIndex = 14;
			this->button14->Text = L"6";
			this->button14->UseVisualStyleBackColor = false;
			this->button14->Click += gcnew System::EventHandler(this, &Calculator::button14_Click);
			// 
			// button15
			// 
			this->button15->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 16.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button15->Location = System::Drawing::Point(349, 257);
			this->button15->Name = L"button15";
			this->button15->Size = System::Drawing::Size(62, 53);
			this->button15->TabIndex = 15;
			this->button15->Text = L"-";
			this->button15->UseVisualStyleBackColor = true;
			this->button15->Click += gcnew System::EventHandler(this, &Calculator::button15_Click);
			// 
			// button17
			// 
			this->button17->BackColor = System::Drawing::SystemColors::Window;
			this->button17->Location = System::Drawing::Point(99, 338);
			this->button17->Name = L"button17";
			this->button17->Size = System::Drawing::Size(62, 53);
			this->button17->TabIndex = 17;
			this->button17->Text = L"1";
			this->button17->UseVisualStyleBackColor = false;
			this->button17->Click += gcnew System::EventHandler(this, &Calculator::button17_Click);
			// 
			// button18
			// 
			this->button18->BackColor = System::Drawing::SystemColors::Window;
			this->button18->Location = System::Drawing::Point(183, 338);
			this->button18->Name = L"button18";
			this->button18->Size = System::Drawing::Size(62, 53);
			this->button18->TabIndex = 18;
			this->button18->Text = L"2";
			this->button18->UseVisualStyleBackColor = false;
			this->button18->Click += gcnew System::EventHandler(this, &Calculator::button18_Click);
			// 
			// button19
			// 
			this->button19->BackColor = System::Drawing::SystemColors::Window;
			this->button19->Location = System::Drawing::Point(262, 338);
			this->button19->Name = L"button19";
			this->button19->Size = System::Drawing::Size(62, 53);
			this->button19->TabIndex = 19;
			this->button19->Text = L"3";
			this->button19->UseVisualStyleBackColor = false;
			this->button19->Click += gcnew System::EventHandler(this, &Calculator::button19_Click);
			// 
			// button20
			// 
			this->button20->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 13.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button20->ImageAlign = System::Drawing::ContentAlignment::TopCenter;
			this->button20->Location = System::Drawing::Point(349, 338);
			this->button20->Name = L"button20";
			this->button20->Size = System::Drawing::Size(62, 53);
			this->button20->TabIndex = 20;
			this->button20->Text = L"+";
			this->button20->UseVisualStyleBackColor = true;
			this->button20->Click += gcnew System::EventHandler(this, &Calculator::button20_Click);
			// 
			// button22
			// 
			this->button22->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button22->Location = System::Drawing::Point(12, 413);
			this->button22->Name = L"button22";
			this->button22->Size = System::Drawing::Size(62, 53);
			this->button22->TabIndex = 22;
			this->button22->Text = L"±";
			this->button22->UseVisualStyleBackColor = true;
			this->button22->Click += gcnew System::EventHandler(this, &Calculator::button22_Click);
			// 
			// button23
			// 
			this->button23->BackColor = System::Drawing::SystemColors::Window;
			this->button23->Location = System::Drawing::Point(99, 413);
			this->button23->Name = L"button23";
			this->button23->Size = System::Drawing::Size(146, 53);
			this->button23->TabIndex = 23;
			this->button23->Text = L"0";
			this->button23->UseVisualStyleBackColor = false;
			this->button23->Click += gcnew System::EventHandler(this, &Calculator::button23_Click);
			// 
			// button24
			// 
			this->button24->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 13.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button24->Location = System::Drawing::Point(262, 413);
			this->button24->Name = L"button24";
			this->button24->Size = System::Drawing::Size(62, 53);
			this->button24->TabIndex = 24;
			this->button24->Text = L"․";
			this->button24->UseVisualStyleBackColor = true;
			this->button24->Click += gcnew System::EventHandler(this, &Calculator::button24_Click);
			// 
			// button25
			// 
			this->button25->Font = (gcnew System::Drawing::Font(L"Microsoft YaHei", 18, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->button25->Location = System::Drawing::Point(349, 413);
			this->button25->Name = L"button25";
			this->button25->Size = System::Drawing::Size(62, 53);
			this->button25->TabIndex = 25;
			this->button25->Text = L"⁼";
			this->button25->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->button25->UseVisualStyleBackColor = true;
			this->button25->Click += gcnew System::EventHandler(this, &Calculator::button25_Click);
			// 
			// Calculator
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(423, 494);
			this->Controls->Add(this->button25);
			this->Controls->Add(this->button24);
			this->Controls->Add(this->button23);
			this->Controls->Add(this->button22);
			this->Controls->Add(this->button20);
			this->Controls->Add(this->button19);
			this->Controls->Add(this->button18);
			this->Controls->Add(this->button17);
			this->Controls->Add(this->button15);
			this->Controls->Add(this->button14);
			this->Controls->Add(this->button13);
			this->Controls->Add(this->button12);
			this->Controls->Add(this->button10);
			this->Controls->Add(this->button9);
			this->Controls->Add(this->button8);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->display);
			this->Name = L"Calculator";
			this->Text = L"Calculator";
			this->ResumeLayout(false);

		}
		double firstnum;
		double secondnum;
		double result;
		char operation;
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		firstnum = Convert::ToInt32(display->Text);
		display->Text = "0";
		operation = '%';
	}
	private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) {
		if (display->Text == "0") {
			display->Text = "7";
		}
		else {
			display->Text = Convert::ToInt32(display->Text) + "7";
		}
	}
private: System::Void button17_Click(System::Object^  sender, System::EventArgs^  e) {
	if (display->Text == "0") {
		display->Text = "1";
	}
	else {
		display->Text = Convert::ToInt32(display->Text) + "1";
	}
}
private: System::Void button18_Click(System::Object^  sender, System::EventArgs^  e) {
	if (display->Text == "0") {
		display->Text = "2";
	}
	else {
		display->Text = Convert::ToInt32(display->Text) + "2";
	}
}
private: System::Void button19_Click(System::Object^  sender, System::EventArgs^  e) {
	if (display->Text == "0") {
		display->Text = "3";
	}
	else {
		display->Text = Convert::ToInt32(display->Text) + "3";
	}
}
private: System::Void button23_Click(System::Object^  sender, System::EventArgs^  e) {
	if (display->Text == "0") {
		display->Text = "0";
	}
	else {
		display->Text = Convert::ToInt32(display->Text) + "0";
	}
}
private: System::Void button12_Click(System::Object^  sender, System::EventArgs^  e) {
	if (display->Text == "0") {
		display->Text = "4";
	}
	else {
		display->Text = Convert::ToInt32(display->Text) + "4";
	}
}
private: System::Void button13_Click(System::Object^  sender, System::EventArgs^  e) {
	if (display->Text == "0") {
		display->Text = "5";
	}
	else {
		display->Text = Convert::ToInt32(display->Text) + "5";
	}
}
private: System::Void button14_Click(System::Object^  sender, System::EventArgs^  e) {
	if (display->Text == "0") {
		display->Text = "6";
	}
	else {
		display->Text = Convert::ToInt32(display->Text) + "6";
	}
}
private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e) {
	if (display->Text == "0") {
		display->Text = "8";
	}
	else {
		display->Text = Convert::ToInt32(display->Text) + "8";
	}
}
private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e) {
	if (display->Text == "0") {
		display->Text = "9";
	}
	else {
		display->Text = Convert::ToInt32(display->Text) + "9";
	}
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	display->Text = "";
	display->Text = "0";
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	display->Text = "";
	display->Text = "0";
}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {

	firstnum = Convert::ToInt32(display->Text);
	display->Text= "0";
	operation = '÷';



}
private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e) {
	firstnum = Convert::ToInt32(display->Text);
	display->Text = "0";
	operation = 'x';
}
private: System::Void button15_Click(System::Object^  sender, System::EventArgs^  e) {
	firstnum = Convert::ToInt32(display->Text);
	display->Text = "0";
	operation = '-';
}
private: System::Void button20_Click(System::Object^  sender, System::EventArgs^  e) {
	firstnum = Convert::ToInt32(display->Text);
	display->Text = "0";
	operation = '+';
}
private: System::Void button25_Click(System::Object^  sender, System::EventArgs^  e) {
	secondnum = Convert::ToInt32(display->Text);
	switch (operation) {
	case '+': result = firstnum + secondnum;
		      display->Text = System::Convert::ToString(result);
		      break;
	case '-': result = firstnum - secondnum;
		display->Text = System::Convert::ToString(result);
		break;
	case 'x': result = firstnum * secondnum;
		display->Text = System::Convert::ToString(result);
		break;
	case '÷': result = firstnum / secondnum;
		display->Text = System::Convert::ToString(result);
		break;
	
	case '±': result = - firstnum;
		display->Text = System::Convert::ToString(result);
		break;
	case '.': result = (firstnum*10+secondnum)/10;
		display->Text = System::Convert::ToString(result);
		break;
	
	case '√': result = sqrt(firstnum);
		display->Text = System::Convert::ToString(result);
		break;
	case '%': result = firstnum/100;
		display->Text = System::Convert::ToString(result);
		break;
	}
}
private: System::Void button24_Click(System::Object^  sender, System::EventArgs^  e) {
	firstnum = Convert::ToInt32(display->Text);
	display->Text = "0";
	operation = '.';
}
private: System::Void button22_Click(System::Object^  sender, System::EventArgs^  e) {
		firstnum = Convert::ToInt32(display->Text);
		display->Text = "0";
	operation = '±';
}
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
	firstnum = Convert::ToInt32(display->Text);
	display->Text = "0";
	operation = '√';
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	firstnum = Convert::ToInt32(display->Text);
	display->Text = "0";
	operation = '<-';
}
};
}
